import Vue from 'vue'
import App from './App.vue'
import VeeValidate, {
  Validator
} from 'vee-validate'
import es from 'vee-validate/dist/locale/es'

const Veeconfig = {
  locale: 'es_ES',
  events: 'blur'
};

Validator.localize({
  es_ES: es
});

Vue.use(VeeValidate, Veeconfig)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
