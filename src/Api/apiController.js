import axios from 'axios';
const API_URL = 'http://localhost:3000';

export default class APIService {
    constructor() {}
    addUser(newProd) {
        return axios.post(API_URL + '/usuarios', newProd)
    }
    findUsername(username) {
        return axios.get(API_URL + '/usuarios/?usuario=' + username)
    }

}
